#####################
#### CTYPE: TEXT ####
#####################

tt_content.text >
tt_content.text = FLUIDTEMPLATE
tt_content.text {

    ################
    ### TEMPLATE ###
    ################
    templateName = Text
    templateRootPaths {
        0 = EXT:theme/Resources/Private/Templates/ContentElements/
        10 = {$plugin.theme_contentelements.view.templateRootPath}
    }
    partialRootPaths {
        0 = EXT:theme/Resources/Private/Partials/ContentElements/
        10 = {$plugin.theme_contentelements.view.partialRootPath}
    }
    layoutRootPaths {
        0 = EXT:theme/Resources/Private/Layouts/ContentElements/
        10 = {$plugin.theme_contentelements.view.layoutRootPath}
    }

}
