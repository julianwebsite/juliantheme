<?php

/************************************************************************
 * Extension Manager/Repository config file for ext "theme".
 ************************************************************************/
$EM_CONF[$_EXTKEY] = array(
    'title' => 'Julian Theme',
    'description' => 'Theme delivers a full configured frontend theme for TYPO3, based on the Bootstrap CSS Framework.',
    'category' => 'templates',
    'constraints' => array(
        'depends' => array(
            'typo3' => '7.6.2-8.99.99'
        ),
        'conflicts' => array(
            'css_styled_content' => '*',
            'fluid_styled_content' => '*',
            'themes' => '*',
            'fluidpages' => '*',
            'dyncss' => '*',
        ),
    ),
    'autoload' => array(
        'psr-4' => array(
            'Julian\\Theme\\' => 'Classes'
        ),
    ),
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Julian',
    'author_email' => 'info@julian.info',
    'author_company' => 'private',
    'version' => '1.0.1',
);
