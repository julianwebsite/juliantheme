﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _start:

=============================================================
Theme
=============================================================

.. only:: html

    :Classification:
        theme

    :Version:
        |release|

    :Language:
        en

    :Description:
        Theme delivers a full configured frontend theme for TYPO3, based on the Bootstrap CSS Framework.

    :Keywords:
        Theme, Bootstrap

    :Copyright:
        2014

    :Author:
        Julian

    :Email:
        info@julian.info

    :License:
        This document is published under the Open Content License
        available from http://www.opencontent.org/opl.shtml

    :Rendered:
        |today|

    The content of this document is related to TYPO3,
    a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.


    **Table of Contents**

.. toctree::
    :maxdepth: 5
    :titlesonly:
    :glob:

    Introduction/Index
    Configuration/Index
    AdministratorManual/Index